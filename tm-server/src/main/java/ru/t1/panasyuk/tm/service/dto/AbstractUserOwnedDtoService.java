package ru.t1.panasyuk.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.api.repository.dto.IUserOwnedDtoRepository;
import ru.t1.panasyuk.tm.api.service.IConnectionService;
import ru.t1.panasyuk.tm.api.service.dto.IUserOwnedDtoService;
import ru.t1.panasyuk.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.t1.panasyuk.tm.exception.entity.EntityNotFoundException;
import ru.t1.panasyuk.tm.exception.field.IdEmptyException;
import ru.t1.panasyuk.tm.exception.field.IndexIncorrectException;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

public abstract class AbstractUserOwnedDtoService<M extends AbstractUserOwnedModelDTO, R extends IUserOwnedDtoRepository<M>>
        implements IUserOwnedDtoService<M> {

    @NotNull
    final IConnectionService connectionService;

    @NotNull
    protected abstract R getRepository(@NotNull final EntityManager entityManager);

    public AbstractUserOwnedDtoService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @Override
    public M add(@NotNull final String userId, @Nullable final M model) {
        if (model == null) return null;
        model.setUserId(userId);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final R repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return model;
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final R repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final R repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existsById(@NotNull final String userId, @Nullable final String id) {
        boolean result;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final R repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            result = repository.findOneById(userId, id) != null;
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            entityManager.close();
        }
        return result;
    }

    @Nullable
    @Override
    public List<M> findAll() {
        @Nullable final List<M> models;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final R repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            models = repository.findAll();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            entityManager.close();
        }
        return models;
    }

    @Nullable
    @Override
    public List<M> findAll(@NotNull final String userId) {
        @Nullable final List<M> models;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final R repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            models = repository.findAll(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            entityManager.close();
        }
        return models;
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String userId, @Nullable final String id) {
        if (id == null) return null;
        @Nullable final M model;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final R repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            model = repository.findOneById(userId, id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            entityManager.close();
        }
        return model;
    }

    @Nullable
    @Override
    public M findOneByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index > getSize(userId)) throw new IndexIncorrectException();
        @Nullable final M model;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final R repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            model = repository.findOneByIndex(userId, index);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            entityManager.close();
        }
        return model;
    }

    @Override
    public int getSize(@NotNull final String userId) {
        int result;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final R repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            result = repository.getSize(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            entityManager.close();
        }
        return result;
    }

    @Nullable
    @Override
    public M removeById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final M removedModel;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final R repository = getRepository(entityManager);
            removedModel = repository.findOneById(userId, id);
            if (removedModel == null) throw new EntityNotFoundException();
            entityManager.getTransaction().begin();
            repository.remove(removedModel);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return removedModel;
    }

    @Nullable
    @Override
    public M removeByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index > getSize(userId)) throw new IndexIncorrectException();
        @Nullable final M removedModel;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final R repository = getRepository(entityManager);
            removedModel = repository.findOneByIndex(userId, index);
            if (removedModel == null) throw new EntityNotFoundException();
            entityManager.getTransaction().begin();
            repository.remove(removedModel);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return removedModel;
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull final Collection<M> models) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final R repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.clear();
            for (@NotNull final M model : models)
                repository.add(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        return models;
    }

    @Override
    public void update(@NotNull final M model) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final R repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
    }

}