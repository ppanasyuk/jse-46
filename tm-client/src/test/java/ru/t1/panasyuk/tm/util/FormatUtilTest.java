package ru.t1.panasyuk.tm.util;

import io.qameta.allure.junit4.DisplayName;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.panasyuk.tm.marker.UnitCategory;

@Category(UnitCategory.class)
@DisplayName("Тестирование утилиты форматирования")
public class FormatUtilTest {

    @Test
    @DisplayName("Преобразование байтов в строку")
    public void formatBytesTest() {
        long bytes = 1;
        @NotNull String result = "";
        for (int i = 0; i <= 5; i++) {
            result = FormatUtil.formatBytes(bytes);
            Assert.assertFalse(result.isEmpty());
            bytes = bytes * 1024;
        }
    }

}